import connector from './as_connector.js';
const Aerospike = require('aerospike');


export default class AS_DB {
    constructor(ns, set) {
        this.client = connector.client;
        this.ns = ns;
        this.set = set;
        this.query = this.client.query(ns, set);
    }

    insert(key, rec, type = this.set) {
        let k = new Aerospike.Key(this.ns, type, key);
        this.client.put(k, rec, function(error) {
            if (error) {
                console.log('error: %s', error.message)
            } else {
                console.log('Record written to database successfully.')
            }
        })
    }

    read(key, callback) {
        this.client.get(new Aerospike.Key(this.ns, this.set, key), (error, record, metadata) => {
            if (error) {
                switch (error.code) {
                    case Aerospike.status.AEROSPIKE_ERR_RECORD_NOT_FOUND:
                        callback(false);
                        break
                    default:
                        callback(false);
                }
            } else {
                callback(record);
            }
        })
    }

    find_range(bin, start, end, callback) {
        this.query.where(Aerospike.filter.range(bin, start, end));
        this.stream_each(this.query, callback);
    }

    find_equal(bin, eq, callback) {
        this.query.where(Aerospike.filter.equal(bin, eq));
        this.stream_each(this.query, callback);
    }

    find_contains(bin, eq, callback) {
        this.query.where(Aerospike.filter.contains(bin, eq));
        this.stream_each(this.query, callback);
    }

    stream_each(query, callback) {
        var stream = query.foreach()
        stream.on('data', function(record) {
            callback(record);
        })
        stream.on('error', function(error) {
            console.log(error);
        })
        stream.on('end', function() {

        })
    }

    index(bin, datatype) {
        let options = {
            ns: this.ns,
            set: this.set,
            bin: bin,
            index: `idx_${this.ns}_${this.set}_${bin}`,
            datatype: Aerospike.indexDataType[datatype]
        }

        this.client.createIndex(options, function(error, job) {
            if (error) {
                console.log('\x1b[41m', 'Error' ,'\x1b[0m');
                console.log(error);
            }

            job.waitUntilDone(function(error) {
                console.log('\x1b[42m', 'Index was created successfully' ,'\x1b[0m');
                this.insert(`idx_${this.ns}_${this.set}_${bin}`,{created:1}, 'index');
            })
        })
    }
}
